#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 08:13:20 2024

@author: mbouvier
"""

#TP1 ALN

#Question 1

import scipy.linalg as nla
import numpy as np

A = np.array([[421,-169,-317],[260,91,-357],[362,123,-118]], dtype = np.float64, order = 'F')
b = np.array([-343,356,-197], dtype = np.float64)

m = A.shape[0]

lu,piv = nla.lu_factor(A, overwrite_a = True)

lignes = [i for i in range(0, m)]
for i in range(0, m):
    lignes[i], lignes[piv[i]], = lignes[piv[i]], lignes[i]
    
Pb = b[lignes]

nla.solve_triangular(A, Pb, lower = True, unit_diagonal = True, overwrite_b = True)
nla.solve_triangular(A, Pb, lower = False, overwrite_b = False)

#Question 2

def my_lu_factor(A, m):
    for k in range(m-1):
        maxelt = abs(A[k][k])
        ipiv[k] = k
        for i in range(k+1, m):
            if abs(A[i][k]) > maxelt:
                maxelt = abs(A[i][k])
                ipiv[k] = i
        if maxelt == 0:
            raise ValueError('Matrice singulière')
        for j in range(m):
            tmp = A[k][j]
            A[k][j] = A[ipiv[k]][j]
            A[ipiv[k]][j] = tmp
        for i in range(k+1, m):
            A[i][k] = A[i][k] / A[k][k]
        for j in range(k+1, m):
            for i in range(k+1, m):
                A[i][j] = A[i][j] - A[i][k]*A[k][j]
    return A, ipiv
        
#Question 3

A = np.array([[421,-169,-317],[260,91,-357],[362,123,-118]], dtype = np.float64, order = 'F')

m = A.shape[0]

ipiv = [0]*(m-1)

my_lu_factor(A, m, ipiv)

    