import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

 #q1
 
x = np.array([0.12, 0.15, 0.9, 2.1, 2.3], dtype = np.float64)
b = np.array([33, 27, 5, -1 , -2], dtype = np.float64)

A = np.zeros((len(x), 3), dtype = np.float64)

for i in range(len(x)):
    A[i][0] = x[i]
    A[i][1] = 1/x[i]
    A[i][2] = x[i]**2

Q,R = np.linalg.qr(A, mode = "reduced")
Qt = np.transpose(Q)
Qtb = np.dot(Qt, b)
v = nla.solve_triangular(R, Qtb)

def f(x):
    return v[0]*x + v[1]*(1/x) + v[2]*(x**2)

xplot = np.linspace(0.05, 3)
yplot = np.array([f(x) for x in xplot])
plt.plot(xplot, yplot)
plt.scatter(x,b)
plt.show()

#q2

def my_qr(A):
    m, n = A.shape
    x = A[0:m,0]  # A[:,0]
    nrm2 = np.linalg.norm (x, 2)
    v = x.copy ()
    v[0] += nrm2

    F = np.eye (m) - (2/np.dot(v,v)) * np.outer (v,v)
    Q = F
    
    for i in range(1,m-1):
    
        A2 = np.dot (Q, A)
        x = A2[i:m,i]
        nrm2 = np.linalg.norm (x, 2)
        v = x.copy ()
        v[0] += nrm2

        F = np.eye (m-i) - (2/np.dot(v,v)) * np.outer (v,v)
        Q2 = np.eye (m)

        Q = np.dot(Q, Q2)
    
    
    R = np.dot (Q, A2)
    
    return(Q,R)

A = np.array ([[1, -1, 4], [1, 4, -2], [1, 4, 2], [1, -1, 0]], dtype=np.float64)

my_qr(A)