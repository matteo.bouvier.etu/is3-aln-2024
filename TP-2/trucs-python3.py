import numpy as np
import fractions

# Pour convertir un flottant en fraction.
# Attention : algorithme instable

def approx2(c, maxd):
    return fractions.Fraction.from_float(c).limit_denominator(maxd)

# Pour éviter certains problèmes d'affichage

np.set_printoptions(linewidth=160)

