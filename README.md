# is3-aln-2024

1. Faire un fork du projet francois.boulier/is3-aln-2024.git

2. Rendre votre projet privé.
   Rajouter les enseignants François Boulier et Louis Roussel comme
    membres avec le statut Maintainer et une date d'expiration au 15/07/2024

3. Sur une machine de TP :

    3.a git clone <url-de-votre-dépôt>
    3.b exécuter ./my-git-config dans la copie de travail créée par (3.a)

