#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 08:06:18 2024

@author: mbouvier
"""

import scipy.linalg as nla
import numpy as np
from TP1 import my_lu_factor
epsilon = 0.000000021


#seule et unique question :

#version avec nla.lu :   

 #def puissance_inverse(A, mu):  #mu vap de A
  #  n = A.shape[0]
   # B = A - mu * np.identity(n)
    #P, L, U = nla.lu(B)
    #v = np.random.randn(3)
    #fini = False
    #while (fini == False):
     #   w = nla.solve_triangular(L, v, lower=True)  #pas besoin de P
      #  w = nla.solve_triangular(U, w, lower=False)
       # w = w/nla.norm(w,2)
        #erreur = 1 - abs(np.dot(np.transpose(w),v))
        #v = w
        #fini = erreur < epsilon #au fil des iterations, l'erreur se rapproche de 0, donc l'algo sarrete quand l'erreur devient moindre
    #return v
        
#version avec my_lu_factor du tp1 :
    
def puissance_inverse2(A, mu):
    n = A.shape[0]
    B = A - mu * np.identity(n)
    LU, P = my_lu_factor(B, mu)
    v = np.random.randn(3)
    fini = False
    while(fini == False):
        w = nla.solve_triangular(np.lower.tri(LU))

