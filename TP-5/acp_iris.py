import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

#   0:50 Iris-setosa
#  50:100 Iris-versicolor
# 100:150 Iris-virginica

# colonne : sepal length, sepal width, petal length, petal width
A = np.array([
[5.1,3.5,1.4,0.2],
[4.9,3.0,1.4,0.2],
[4.7,3.2,1.3,0.2],
[4.6,3.1,1.5,0.2],
[5.0,3.6,1.4,0.2],
[5.4,3.9,1.7,0.4],
[4.6,3.4,1.4,0.3],
[5.0,3.4,1.5,0.2],
[4.4,2.9,1.4,0.2],
[4.9,3.1,1.5,0.1],
[5.4,3.7,1.5,0.2],
[4.8,3.4,1.6,0.2],
[4.8,3.0,1.4,0.1],
[4.3,3.0,1.1,0.1],
[5.8,4.0,1.2,0.2],
[5.7,4.4,1.5,0.4],
[5.4,3.9,1.3,0.4],
[5.1,3.5,1.4,0.3],
[5.7,3.8,1.7,0.3],
[5.1,3.8,1.5,0.3],
[5.4,3.4,1.7,0.2],
[5.1,3.7,1.5,0.4],
[4.6,3.6,1.0,0.2],
[5.1,3.3,1.7,0.5],
[4.8,3.4,1.9,0.2],
[5.0,3.0,1.6,0.2],
[5.0,3.4,1.6,0.4],
[5.2,3.5,1.5,0.2],
[5.2,3.4,1.4,0.2],
[4.7,3.2,1.6,0.2],
[4.8,3.1,1.6,0.2],
[5.4,3.4,1.5,0.4],
[5.2,4.1,1.5,0.1],
[5.5,4.2,1.4,0.2],
[4.9,3.1,1.5,0.1],
[5.0,3.2,1.2,0.2],
[5.5,3.5,1.3,0.2],
[4.9,3.1,1.5,0.1],
[4.4,3.0,1.3,0.2],
[5.1,3.4,1.5,0.2],
[5.0,3.5,1.3,0.3],
[4.5,2.3,1.3,0.3],
[4.4,3.2,1.3,0.2],
[5.0,3.5,1.6,0.6],
[5.1,3.8,1.9,0.4],
[4.8,3.0,1.4,0.3],
[5.1,3.8,1.6,0.2],
[4.6,3.2,1.4,0.2],
[5.3,3.7,1.5,0.2],
[5.0,3.3,1.4,0.2],
[7.0,3.2,4.7,1.4],
[6.4,3.2,4.5,1.5],
[6.9,3.1,4.9,1.5],
[5.5,2.3,4.0,1.3],
[6.5,2.8,4.6,1.5],
[5.7,2.8,4.5,1.3],
[6.3,3.3,4.7,1.6],
[4.9,2.4,3.3,1.0],
[6.6,2.9,4.6,1.3],
[5.2,2.7,3.9,1.4],
[5.0,2.0,3.5,1.0],
[5.9,3.0,4.2,1.5],
[6.0,2.2,4.0,1.0],
[6.1,2.9,4.7,1.4],
[5.6,2.9,3.6,1.3],
[6.7,3.1,4.4,1.4],
[5.6,3.0,4.5,1.5],
[5.8,2.7,4.1,1.0],
[6.2,2.2,4.5,1.5],
[5.6,2.5,3.9,1.1],
[5.9,3.2,4.8,1.8],
[6.1,2.8,4.0,1.3],
[6.3,2.5,4.9,1.5],
[6.1,2.8,4.7,1.2],
[6.4,2.9,4.3,1.3],
[6.6,3.0,4.4,1.4],
[6.8,2.8,4.8,1.4],
[6.7,3.0,5.0,1.7],
[6.0,2.9,4.5,1.5],
[5.7,2.6,3.5,1.0],
[5.5,2.4,3.8,1.1],
[5.5,2.4,3.7,1.0],
[5.8,2.7,3.9,1.2],
[6.0,2.7,5.1,1.6],
[5.4,3.0,4.5,1.5],
[6.0,3.4,4.5,1.6],
[6.7,3.1,4.7,1.5],
[6.3,2.3,4.4,1.3],
[5.6,3.0,4.1,1.3],
[5.5,2.5,4.0,1.3],
[5.5,2.6,4.4,1.2],
[6.1,3.0,4.6,1.4],
[5.8,2.6,4.0,1.2],
[5.0,2.3,3.3,1.0],
[5.6,2.7,4.2,1.3],
[5.7,3.0,4.2,1.2],
[5.7,2.9,4.2,1.3],
[6.2,2.9,4.3,1.3],
[5.1,2.5,3.0,1.1],
[5.7,2.8,4.1,1.3],
[6.3,3.3,6.0,2.5],
[5.8,2.7,5.1,1.9],
[7.1,3.0,5.9,2.1],
[6.3,2.9,5.6,1.8],
[6.5,3.0,5.8,2.2],
[7.6,3.0,6.6,2.1],
[4.9,2.5,4.5,1.7],
[7.3,2.9,6.3,1.8],
[6.7,2.5,5.8,1.8],
[7.2,3.6,6.1,2.5],
[6.5,3.2,5.1,2.0],
[6.4,2.7,5.3,1.9],
[6.8,3.0,5.5,2.1],
[5.7,2.5,5.0,2.0],
[5.8,2.8,5.1,2.4],
[6.4,3.2,5.3,2.3],
[6.5,3.0,5.5,1.8],
[7.7,3.8,6.7,2.2],
[7.7,2.6,6.9,2.3],
[6.0,2.2,5.0,1.5],
[6.9,3.2,5.7,2.3],
[5.6,2.8,4.9,2.0],
[7.7,2.8,6.7,2.0],
[6.3,2.7,4.9,1.8],
[6.7,3.3,5.7,2.1],
[7.2,3.2,6.0,1.8],
[6.2,2.8,4.8,1.8],
[6.1,3.0,4.9,1.8],
[6.4,2.8,5.6,2.1],
[7.2,3.0,5.8,1.6],
[7.4,2.8,6.1,1.9],
[7.9,3.8,6.4,2.0],
[6.4,2.8,5.6,2.2],
[6.3,2.8,5.1,1.5],
[6.1,2.6,5.6,1.4],
[7.7,3.0,6.1,2.3],
[6.3,3.4,5.6,2.4],
[6.4,3.1,5.5,1.8],
[6.0,3.0,4.8,1.8],
[6.9,3.1,5.4,2.1],
[6.7,3.1,5.6,2.4],
[6.9,3.1,5.1,2.3],
[5.8,2.7,5.1,1.9],
[6.8,3.2,5.9,2.3],
[6.7,3.3,5.7,2.5],
[6.7,3.0,5.2,2.3],
[6.3,2.5,5.0,1.9],
[6.5,3.0,5.2,2.0],
[6.2,3.4,5.4,2.3],
[5.9,3.0,5.1,1.8]], dtype=np.float64)


#TP4

#Question 1 :

M = np.random.random([4,4])
Q, R = nla.qr(M)
L = np.diag([7.1, 6.9, 5.1, 1.2])
A = np.dot(Q, np.dot(L, np.transpose(Q)))
H = nla.hessenberg(A)
"""
fabrique une matrice H symétrique, tridiagonale dont on connaît les valeurs propres : 7.1, 6.9, 5.1, 1.2

M = matrice aléatoire
Q = la matrice Q de la factorisation QR
  = matrice orthogonale aléatoire

A = Q*lambda*Qt         lambda diagonale fixé par nous
  = matrice aléatoire supérieure dont les valeurs propres sont connues
"""


#Question 2 :
def sturm(A, mu):
    m = A.shape[0]
    T = np.empty(m+1, dtype=np.float64)
    T[0] = 1
    T[1] = A[0,0] - mu
    T[2] = (A[1,1] - mu)*T[1] - A[1,0]**2
    for k in range(2, m):
        T[k+1] = (A[k,k] - mu)*T[k] - A[k, k-1]**2*T[k-1]
    return T


#Question 3 :
def V(T):
    nb_sign = 0
    for i in range(1, len(T)):
        if(T[i] == 0 and T[i-1]!=0 and T[i] >= 0):
            nb_sign = nb_sign
        else:
            if(np.sign(T[i]) != np.sign(T[i-1])):
                nb_sign += 1
    return nb_sign


#Question 4 :
def isolate(A, mu1, mu2):
    n1 = V(sturm(A, mu1))
    n2 = V(sturm(A, mu2))
    n = n2-n1
    if(n==0):
        return []
    elif(n==1):
        return [[mu1, mu2]]
    else:
        m = (mu1+mu2)/2
        return(isolate(A, mu1, m) + isolate(A, m, mu2))


#Question 5 :
def Gershgorin(A):
    born_min = A[0,0] - abs(A[0,1])
    born_max = A[0,0] + abs(A[0,1])
    tmp_min = 0
    tmp_max = 0
    m = A.shape[0]
    for i in range(1, m-1):
        tmp_min = A[i,i] - (abs(A[i,i-1]) + abs(A[i,i+1]))
        tmp_max = A[i,i] + (abs(A[i,i-1]) + abs(A[i,i+1]))
        if(tmp_min < born_min):
            born_min = tmp_min
        if(tmp_max > born_max):
            born_max = tmp_max
    tmp_min = A[m-1,m-1] - abs(A[m-1,m-2])
    tmp_max = A[m-1,m-1] + abs(A[m-1,m-2])
    if(tmp_min < born_min):
        born_min = tmp_min
    if(tmp_max > born_max):
        born_max = tmp_max
    return[born_min, born_max]



#Question 6

def isolateV2(A, mu1, mu2, eps):
    n1 = V(sturm(A, mu1))
    n2 = V(sturm(A, mu2))
    n = n2-n1
    if(n==0):
        return []
    elif(n==1 and mu2-mu1 < eps):
        return [[mu1, mu2]]
    else:
        m = (mu1+mu2)/2
        return(isolateV2(A, mu1, m, eps) + isolateV2(A, m, mu2, eps))


def my_eigvalsh(A):
    H = nla.hessenberg(A)
    return isolateV2(H, Gershgorin(H)[0], Gershgorin(H)[1], 1e-6)



np.set_printoptions(linewidth=160)

m, n = A.shape
for k in range (0,n) :
    A[:,k] = A[:,k] - np.average(A[:,k])

# Calcul d'ACP en deux dimensions par la méthode 
#   historique (matrice de covariance + calcul
#   des valeurs propres d'une mat. symétrique)

C = 1/(n-1) * np.dot (np.transpose (A), A)


#Code initiam

lmbda, Q = nla.eigh (C)


#Nouveau code

I = my_eigvalsh(C)

D = []
for i in range(0,4):
    lambd = I[i][1] - I[i][0] + I[i][1]
    D.append(lambd)

lamb = np.diag(D)


Q_max = np.empty ([n,2], dtype=np.float64)
Q_max[:,0] = Q[:,3]
Q_max[:,1] = Q[:,2]

# Chaque iris (= ligne de A) est un point dans R**4
# On projette chaque point 
# - dans le plan défini par les deux vecteurs propres
# - orthogonalement
P = np.dot (A, Q_max)

# Visualisation
z = int(m/3)
plt.scatter (P[0:z,0], P[0:z,1], color='blue', marker='o')
plt.scatter (P[z:2*z,0], P[z:2*z,1], color='green', marker='*')
plt.scatter (P[2*z:3*z,0], P[2*z:3*z,1], color='red', marker='s')
plt.show ()

# Même calcul d'ACP par la méthode moderne 
#   (SVD de la matrice A sans passer par la
#    matrice de covariance)

U, Sigma, Vt = nla.svd(A, full_matrices=False)
V_max = np.transpose (Vt[0:2,:])

# V_max contient les deux vecteurs singuliers droits
#   associés aux deux valeurs singulières les plus
#   grandes. Rq : V_max = Q_max !

# On projette
P = np.dot (A, V_max)

# Visualisation
plt.scatter (P[0:z,0], P[0:z,1], color='blue', marker='o')
plt.scatter (P[z:2*z,0], P[z:2*z,1], color='green', marker='*')
plt.scatter (P[2*z:3*z,0], P[2*z:3*z,1], color='red', marker='s')
