#TP4 ALN

#Question 1

import numpy as np
import scipy.linalg as nla

M = np.random.random([4,4])
Q,R = nla.qr(M)
L = np.diag([7.1, 6.9, 5.1, 1.2])
A = np.dot(Q, np.dot(L, np.transpose(Q)))
H = nla.hessenberg(A)

#Fabrique une matrice H symetrique, tridiagonale dont on connait les valeurs propres
#7.1, 6.9, 5.1, 1.2
#M : matrice aleatoire
#Q : la matrice Q de la factorisation QR de M
#  : matrice orthogonale aleatoire
#A = Q*lambda*Qt symetrique, admet les elements de lambda pour valeurs propres
#  : matrice aleatoire symetrique dont les valeurs propres sont connues
#lambda diagonale fixee par nous

#Question 2

def Sturm(mu, A): #A doit etre tridiagonale symetrique
    n = A.shape[0]
    T = np.empty(n+1, dtype = np.float64)
    T[0] = 1
    T[1] = A[0,0]
    T[2] = (A[1,1] - mu)*T[1] - (A[1,0]**2)*T[0]
    for k in range(2,n):
        T[k+1] = (A[k,k] - mu)*T[k] - (A[k, k-1]**2)*T[k-1]
    return(T)

#On observe que cet algorithme renvoie une suite dont le nombre de variations de signe a l'interieur equivaut au nombre de valeurs propres inferieures au mu donné en argument

#Question 3

def v(T):   #T doit etre une suite de Sturm
    n = T.shape
    nb_var = 0
    for k in range(1,n):
        if (T[k]*T[k-1] == 0):
            nb_var +=1
    return(nb_var)

#Question 4

def isolate(A, mu1, mu2): #A doit etre tridiagonale symetrique
    n1 = v(Sturm(mu1, A))
    n2 = v(Sturm(mu2, A))
    n = n2 - n1
    if (n == 0):
        return[]
    else :
        n = (n2 - n1)/2 
        return isolate(A, mu1, n)
    
#Question 5

def Gershgorin(A):
    born_min = A[0,0] - abs(A[0,1])
    born_max = A[0,0] + abs(A[0,1])
    tmp_min = 0
    tmp_max = 0
    m = A.shape[0]
    for i in range(1, m-1):
        tmp_min = A[i,i] - (abs(A[i,i-1]) + abs(A[i,i+1]))
        tmp_max = A[i,i] + (abs(A[i,i-1]) + abs(A[i,i+1]))
        if(tmp_min < born_min):
            born_min = tmp_min
        if(tmp_max > born_max):
            born_max = tmp_max
    tmp_min = A[m-1,m-1] - abs(A[m-1,m-2])
    tmp_max = A[m-1,m-1] + abs(A[m-1,m-2])
    if(tmp_min < born_min):
        born_min = tmp_min
    if(tmp_max > born_max):
        born_max = tmp_max
    return[born_min, born_max]

#Question 6

def isolateV2(A, mu1, mu2, eps):
    n1 = v(Sturm(A, mu1))
    n2 = v(Sturm(A, mu2))
    n = n2-n1
    if(n==0):
        return []
    elif(n==1 and mu2-mu1 < eps):
        return [[mu1, mu2]]
    else:
        m = (mu1+mu2)/2
        return(isolateV2(A, mu1, m, eps) + isolateV2(A, m, mu2, eps))


def my_eigvalsh(A):
    H = nla.hessenberg(A)
    return isolateV2(H, Gershgorin(H)[0], Gershgorin(H)[1], 1e-6)

