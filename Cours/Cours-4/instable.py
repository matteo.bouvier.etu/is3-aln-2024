import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

# Cet exemple montre le lien entre valeurs singulières de A
# et condition de A - calculée avec la norme 2 : kappa(A) = sigma[1]/sigma[n]

A = np.array ([[2,4,5],[-1,3,3],[1,0,-1]], dtype=np.float64)
A
Amu = nla.inv (A)
Amu
nla.norm(A,2) * nla.norm (Amu,2)
sigma = nla.svdvals(A)
sigma
sigma[0]/sigma[2]

# Calcul des valeurs propres de A par un algorithme instable
# Perturbation de l'ordre de sqrt(epsilon-machine) attendue !

for k in range (15, 1, -1) :
    delta = 10**(-k)
    A = np.array ([[1+delta, 0], [0, 1]], dtype=np.float64)
    x = np.array ([1+delta, 1], dtype=np.float64)
    x = np.sort (x)
# algorithme instable pour le calcul des valeurs propres de A
    P = np.array ([1, -(2+delta), 1+delta], dtype=np.float64)
    xbar = np.roots (P)
# il existe un algorithme backward stable pour le calcul des valeurs propres
# pour l'obtenir, décommenter la ligne suivante :
#   xbar = nla.eigvalsh (A)
    xbar = np.sort (xbar)
    errrel = nla.norm (x-xbar,2) / nla.norm (x,2)
    print ("delta = 10**(%2d) - erreur relative = %e" % (-k, errrel))

