import numpy as np
import scipy.linalg as nla

X = np.array ([[1, 3, -2], [12, -4, 1], [3, -13, -1]], dtype=np.float64)
Lambda = np.diag ([-6, 4, 3])

A = np.dot (X, np.dot (Lambda, nla.inv (X)))

# Retourne valeurs propreset matrice X (à une permutation près des colonnes)
Lambda2, X2 = nla.eig (A)
Lambda2
X2

# Méthode de la puissance
# Converge vers un vecteur propre associé à -6
v = np.array ([1, 2, 3], dtype=np.float64)
for k in range (0, 10) :
    v = np.dot (A, v)
    v = 1/nla.norm(v,2) * v

# Méthode de la puissance inverse
# Converge vers un vecteur propre associé à 3

mu = -5.999
mu = 3.01
B = nla.inv (A - mu*np.eye (3))
v = np.array ([1, 2, 3], dtype=np.float64)
for k in range (0, 3) :
    v = np.dot (B, v)
    v = 1/nla.norm(v,2) * v

# Algorithme QR naïf appliqué à A
# Converge vers la matrice T d'une factorisation de Schur de A
# Des approximations des valeurs propres sont sur la diagonale

svg_A = A.copy ()
for k in range (0, 20) :
    Qk, Rk = nla.qr (A)
    A = np.dot (Rk, Qk)



