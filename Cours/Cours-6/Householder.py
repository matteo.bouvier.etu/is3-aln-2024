import numpy as np

A = np.array ([[1, -1, 4], [1, 4, -2], [1, 4, 2], [1, -1, 0]], dtype=np.float64)

m, n = A.shape
x = A[0:m,0]  # A[:,0]
nrm2 = np.linalg.norm (x, 2)
v = x.copy ()
v[0] += nrm2

F = np.eye (m) - (2/np.dot(v,v)) * np.outer (v,v)
Q1 = F

A2 = np.dot (Q1, A)
x = A2[1:m,1]
nrm2 = np.linalg.norm (x, 2)
v = x.copy ()
v[0] += nrm2

F = np.eye (m-1) - (2/np.dot(v,v)) * np.outer (v,v)
Q2 = np.eye (m)
Q2[1:m,1:m] = F
Q2

A3 = np.dot (Q2, A2)
x = A3[2:m,2]
nrm2 = np.linalg.norm (x, 2)
v = x.copy ()
v[0] += nrm2

F = np.eye (m-2) - (2/np.dot(v,v)) * np.outer (v,v)
Q3 = np.eye (m)
Q3[2:m,2:m] = F
Q3

R = np.dot (Q3, A3)
Q = np.transpose (np.dot (Q3, np.dot (Q2, Q1)))


