import numpy as np
import scipy.linalg as nla

A = np.array ([[2, 3, 5], [-4, 1, 2], [3, 3, 0]], dtype=np.float64)
x = np.array ([22, 3, 2024], dtype=np.float64)
b = np.dot (A, x)

Q, R = nla.qr (A)
Q
R

# on vérifie Q orthogonale
np.dot (np.transpose(Q), Q)

# on résout R . x = Q**T . b
# naïvement :
QTb = np.dot (np.transpose (Q), b)
nla.solve_triangular(R, QTb, lower=False)

# plus astucieux :
QTb = np.dot (b, Q)
nla.solve_triangular(R, QTb, lower=False)

# Matrice rectangulaire
A = np.array ([[2, 3, 5], [-4, 1, 2], [3, 3, 0], [2, 1, 2]], dtype=np.float64)
x = np.array ([22, 3, 2024], dtype=np.float64)
b = np.dot (A, x)

# On perturbe un peu le vecteur b
b = np.array([10172.,  3963.25,    74.9,  4098.])
Q, R = nla.qr (A)
Q
R

# Calculons la factorisation réduite
Q1, R1 = nla.qr (A, mode='economic')
Q1
R1
Q1Tb = np.dot (b, Q1)
Q1Tb

# On retrouve presque la date du jour
nla.solve_triangular(R1, Q1Tb)


