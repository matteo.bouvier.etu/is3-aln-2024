Étudiant(e) : MATTEO BOUVIER			

Contrôle Continu
----------------

cc1 : 6.0			moyenne de la classe = 11.6
cc2 : 4.0			moyenne de la classe = 12.6
cc3 : 4.0			moyenne de la classe =  9.3
cc4 : 8.0			moyenne de la classe = 15.6
cc5 : 12.0			moyenne de la classe = 11.9
cc6 : 14.0			moyenne de la classe = 11.6

La note CC est la moyenne des 4 meilleures notes de contrôle continu
CC  : 10.0			moyenne de la classe = 13.4

Commentaire sur le TP1 au 22 mars (je n'ai fait que vérifier la présence d'un rendu)
ton répertoire TP-1 est vide

Présent(e) au TP 1 : oui
Présent(e) au TP 2 : oui
Présent(e) au TP 3 : oui
Présent(e) au TP 4 : oui
Présent(e) au TP 5 : oui

Contrôle TP
-----------

La note TP comprend la note de contrôle TP sur 18 points plus un note
entre 0 et 2 points calculée à partir des relevés de présence lors des TP

TP  : 5.0			moyenne de la classe = 11.6

Devoir Surveillé
----------------

DS  : 7.0			moyenne de la classe = 11.3

